<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\AddEditUser;
use App\User;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Вывод аользователей
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {

        $users = User::with('departments')
            ->orderBy('id')
            ->paginate(4);

        return view('users.users', [
            'users' => $users,
        ]);

    }

    /**
     * Удаление пользователя
     *
     * @param Request $request
     * @return Response
     */
    public function userDelete(Request $request)
    {
        $user = User::find($request->user_id);

        $user->departments()->detach();
        $user->delete();

        $response['success'] = true;

        return ($response);
    }

    /**
     * Редактирование/добавление пользователя
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function userAddEdit(Request $request)
    {
        $user = User::find($request->id);

        return view('users.add_edit', [
            'user' => $user,
        ]);

    }

    /**
     * Сохранение пользователя
     *
     * @param AddEditUser $request
     * @return Response
     */
    public function userSave(AddEditUser $request)
    {
        $user_id       = $request->id;
        $user_name     = $request->name;
        $user_email    = $request->email;
        $user_password = $request->password;

        if ($user_id) {

            $user = User::find($user_id);

            $user->name  = $user_name;
            $user->email = $user_email;

        } else {

            $user = new User([
                'name'     => $user_name,
                'email'    => $user_email,
                'password' => bcrypt($user_password),
            ]);

        }

        $user->save();

        $response['success'] = true;

        return ($response);

    }
}
