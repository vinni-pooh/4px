<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests\Departments\AddEditDepartment;
use App\User;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    /**
     * Вывод отделов
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {

        $departments = Department::with('users')
            ->orderBy('id')
            ->paginate(4);

        return view('departments.departments', [
            'departments' => $departments,
        ]);

    }

    /**
     * Редактирование/добавление отдела
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function departmentAddEdit(Request $request)
    {
        $department_id = $request->id;

        $department       = Department::find($department_id);
        $department_users = User::with(['departments' => function ($query) use ($department_id) {
            $query->where('departments.id', $department_id);
        }])
            ->orderBy('id')
            ->get();

        return view('departments.add_edit', [
            'department'       => $department,
            'department_users' => $department_users,
        ]);

    }

    /**
     * Удаление отдела
     *
     * @param Request $request
     * @return Response
     */
    public function departmentDelete(Request $request)
    {

        $department = Department::find($request->department_id);

        $department->users()->detach();
        Storage::delete($department->logo);
        $department->delete();

        $response['success'] = true;

        return ($response);
    }

    /**
     * Сохранение отдела
     *
     * @param AddEditDepartment $request
     * @return Response
     */
    public function departmentSave(AddEditDepartment $request)
    {
        $department_logo = '';
        if (count($request->file())) {
            $department_logo = $request->attachment->store('logo');
        }

        $department_id          = $request->id;
        $department_name        = $request->name;
        $department_description = $request->description;
        if ($request->selected_users) {
            $selected_users = explode(',', $request->selected_users);
        } else {
            $selected_users = [];
        }

        if ($department_id) {

            $department = Department::find($department_id);

            $department->name        = $department_name;
            $department->description = $department_description;
            if ($department_logo) {
                $department->logo = $department_logo;
            }

        } else {

            $department = new Department([
                'name'        => $department_name,
                'description' => $department_description,
                'logo'        => $department_logo,
            ]);

        }

        DB::transaction(function () use ($department, $selected_users) {
            $department->save();
            $department->users()->sync($selected_users);
        });

        $response['success']       = true;
        $response['department_id'] = $department->id;

        return ($response);

    }
}
