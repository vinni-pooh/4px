<?php

namespace App\Http\Requests\Departments;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;

class AddEditDepartment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (count(Request::file())) {
            return [
                'attachment' => 'image|max:2000',
                'name'       => 'required|string|max:255|unique:departments,name,' . Request::input('id'),
            ];
        } else {
            return [
                'name'       => 'required|string|max:255|unique:departments,name,' . Request::input('id'),
            ];
        }
    }
}
