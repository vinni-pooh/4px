<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
})->middleware('auth');

Auth::routes();

Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'UserController@index')->name('users')->middleware('auth');
    Route::get('/add', 'UserController@userAddEdit')->name('user-add')->middleware('auth');
    Route::post('/user-save', 'UserController@userSave')->name('user-save');
    Route::post('/user-delete', 'UserController@userDelete')->name('user-delete');
    Route::get('/{id}', 'UserController@userAddEdit')->name('user-edit')->middleware('auth');
});

Route::group(['prefix' => 'departments'], function () {
    Route::get('/', 'DepartmentController@index')->name('departments')->middleware('auth');
    Route::get('/add', 'DepartmentController@departmentAddEdit')->name('department-add')->middleware('auth');
    Route::post('/department-save', 'DepartmentController@departmentSave')->name('department-save');
    Route::post('/department-delete', 'DepartmentController@departmentDelete')->name('department-delete');
    Route::get('/{id}', 'DepartmentController@departmentAddEdit')->name('department-edit')->middleware('auth');
});
