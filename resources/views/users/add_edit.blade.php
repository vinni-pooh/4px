@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">

                <user-add-edit
                        :user="{{ json_encode($user) }}"></user-add-edit>


        </div>
    </div>

@endsection
