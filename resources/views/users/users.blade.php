@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h5 class="float-left">Users</h5>
                    <a class="btn btn-sm btn-primary float-right" href="{{ route('user-add') }}" role="button">Add</a>
                </div>
                <div class="card-body">
                    @foreach ($users as $user)
                        <div class="row py-3 border-top">
                            <div class="col-md-3">
                                {{ $user->name }}
                            </div>
                            <div class="col-md-3">
                                {{ $user->email }}
                            </div>
                            <div class="col-md-3">
                                {{ $user->created_at }}
                            </div>
                            <div class="col-md-3">
                                <a class="btn btn-secondary" href="{{ route('user-edit', $user->id) }}" role="button">Edit</a>
                                <user-delete
                                        :user_id="{{ $user->id }}"></user-delete>
                            </div>
                        </div>
                    @endforeach

                    {{ $users->links() }}

                </div>
            </div>

        </div>
    </div>
@endsection
