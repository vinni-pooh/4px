@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h5 class="float-left">Departments</h5>
                    <a class="btn btn-sm btn-primary float-right" href="{{ route('department-add') }}" role="button">Add</a>
                </div>
                <div class="card-body">
                    @foreach ($departments as $department)
                        <div class="row py-3 border-top">
                            <div class="col-md-2">
                                <img class="img-fluid" src="{{ asset($department->logo) }}" alt="">
                            </div>
                            <div class="col-md-5">
                                <strong>{{ $department->name }}</strong><br>
                                {{ $department->description }}
                            </div>
                            <div class="col-md-3">
                                <strong>Users</strong>
                                <ul>
                                    @foreach($department->users as $key=>$user)
                                        <li style="list-style-type: none;">{{ $key+1 }}. {{ $user->name }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-secondary" href="{{ route('department-edit', $department->id) }}" role="button">Edit</a>
                                <department-delete
                                        :department_id="{{ $department->id }}"></department-delete>
                            </div>
                        </div>
                    @endforeach

                    {{ $departments->links() }}

                </div>
            </div>

        </div>
    </div>

@endsection
