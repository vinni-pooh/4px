@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">

                <department-add-edit
                        :users="{{ $department_users }}"
                        :department="{{ json_encode($department) }}"></department-add-edit>


        </div>
    </div>

@endsection
