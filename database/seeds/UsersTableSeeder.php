<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'              => 'admin@test.loc',
            'email'             => 'admin@test.loc',
            'email_verified_at' => now(),
            'password'          => bcrypt('password'),
        ]);

        factory(App\User::class, 14)->create();
    }
}
